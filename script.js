const tabTitles = document.querySelectorAll('.tabs-title');
const tabContent = document.querySelectorAll('.tabs-content li');

function hideAllContentExcept(index) {
    for (let i = 0; i < tabContent.length; i++) {
        if (i !== index) {
            tabContent[i].style.display = 'none';
        } else {
            tabContent[i].style.display = 'block';
        }
    }
}

hideAllContentExcept(0);

tabTitles.forEach((title, index) => {
    title.addEventListener('click', () => {
        hideAllContentExcept(index);
        tabTitles.forEach(t => t.classList.remove('active'));
        title.classList.add('active');
    });
});
